/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrnew.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:51 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:51 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

wchar_t				*ft_wstrnew(size_t size)
{
	wchar_t			*ws;

	if (!(ws = (wchar_t *)malloc(sizeof(wchar_t) * (size + 1))))
		return (NULL);
	while (size > 0)
		*(ws + size--) = '\0';
	*(ws + size--) = '\0';
	return (ws);
}
