/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 16:02:20 by clrichar          #+#    #+#             */
/*   Updated: 2018/02/10 16:02:59 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t				intlen_base(unsigned long long nb,
		unsigned long long base)
{
	size_t					i;

	i = 1;
	while (nb >= base)
	{
		i++;
		nb /= base;
	}
	return (i);
}

static void					base_rot(char **ret)
{
	int						i;

	i = 0;
	while ((*ret)[i])
	{
		if ((*ret)[i] > '9')
			(*ret)[i] = (char)('A' + (*ret)[i] - '9' - 1);
		i++;
	}
}

static void					write_base(char **ret, \
		unsigned long long n, unsigned long long base)
{
	size_t					i;
	size_t					len;

	i = 1;
	len = intlen_base(n, base);
	if ((*ret)[0] == '-')
		len += 1;
	while (n >= base)
	{
		(*ret)[len - i] = (char)((n % base) + '0');
		n /= base;
		i++;
	}
	(*ret)[len - i] = (char)((n % base) + '0');
}

char						*ft_utoa_base(unsigned long long n, long long base)
{
	char					*ret;

	ret = ft_strnew(100);
	write_base(&ret, n, (unsigned long long)base);
	if (base > 10)
		base_rot(&ret);
	return (ret);
}
