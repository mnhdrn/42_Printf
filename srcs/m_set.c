/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_set.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 14:39:46 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/10 21:05:54 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

#define FLAG dna->flag
#define PLUS 0
#define MINUS 1
#define SPACE 2
#define OCT 3
#define HEX 4
#define BIN 5

char				*set_sign(int index)
{
	if (index == PLUS)
		return (ft_strdup("+"));
	if (index == MINUS)
		return (ft_strdup("-"));
	if (index == SPACE)
		return (ft_strdup(" "));
	if (index == OCT)
		return (ft_strdup("0"));
	if (index == HEX)
		return (ft_strdup("0X"));
	if (index == BIN)
		return (ft_strdup("0B"));
	return (NULL);
}

char				*set_size(void)
{
	char			*s;
	long			tmp;
	t_dna			*dna;

	dna = call();
	tmp = (FLAG.v_padding + FLAG.len + FLAG.v_sign);
	FLAG.v_size -= (FLAG.v_size > 0) ? tmp : 0;
	FLAG.v_size = (FLAG.v_size < 0) ? 0 : FLAG.v_size;
	if (!(s = ft_strnew((size_t)FLAG.v_size)))
		return (NULL);
	ft_memset(s, ' ', (size_t)FLAG.v_size);
	return (s);
}

char				*set_padding(void)
{
	char			*s;
	long			tmp;
	t_dna			*dna;

	dna = call();
	if ((ft_strequ(" ", FLAG.s_sign) || ft_strequ("-", FLAG.s_sign) || \
			ft_strequ("+", FLAG.s_sign)) && (FLAG.modifier >> 1 & 1) == 0)
		tmp = FLAG.len;
	else
		tmp = (FLAG.len + FLAG.v_sign);
	FLAG.v_padding -= (FLAG.v_padding > 0) ? tmp : 0;
	FLAG.v_padding = (FLAG.v_padding < 0) ? 0 : FLAG.v_padding;
	if (!(s = ft_strnew((size_t)FLAG.v_padding)))
		return (NULL);
	ft_memset(s, '0', (size_t)FLAG.v_padding);
	return (s);
}

void				set_flag(t_dna *dna)
{
	FLAG.v_sign = (FLAG.s_sign) ? (long)ft_strlen(FLAG.s_sign) : 0;
	FLAG.s_padding = set_padding();
	FLAG.s_size = set_size();
	if ((FLAG.modifier >> 2 & 1) == 1)
	{
		if (dna->flag.s_sign)
			ft_buffer(dna->flag.s_sign, ft_strlen(dna->flag.s_sign));
		if (dna->flag.s_padding)
			ft_buffer(dna->flag.s_padding, ft_strlen(dna->flag.s_padding));
		if (dna->flag.s_flag)
			ft_buffer(dna->flag.s_flag, (size_t)dna->flag.len);
		if (dna->flag.s_size)
			ft_buffer(dna->flag.s_size, ft_strlen(dna->flag.s_size));
	}
	else
	{
		if (dna->flag.s_size)
			ft_buffer(dna->flag.s_size, ft_strlen(dna->flag.s_size));
		if (dna->flag.s_sign)
			ft_buffer(dna->flag.s_sign, ft_strlen(dna->flag.s_sign));
		if (dna->flag.s_padding)
			ft_buffer(dna->flag.s_padding, ft_strlen(dna->flag.s_padding));
		if (dna->flag.s_flag)
			ft_buffer(dna->flag.s_flag, (size_t)dna->flag.len);
	}
}
