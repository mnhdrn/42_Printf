/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 13:56:03 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/10 21:05:52 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void				init(const char *format)
{
	t_dna				*dna;

	dna = call();
	dna->format_len = ft_strlen(format);
	dna->stocked = 0;
	dna->index = 0;
	dna->ret = 0;
	ft_memset(dna->buffer, 0, BUFFER + 1);
}

int						ft_printf(const char *format, ...)
{
	t_dna				*dna;

	dna = call();
	if (!format)
		return (-1);
	else if (ft_strequ(format, ""))
		return (0);
	init(format);
	va_start(dna->va, format);
	ft_parse(format);
	va_end(dna->va);
	if (dna->stocked > 0)
	{
		write(1, dna->buffer, dna->stocked);
		dna->ret += (int)dna->stocked;
		dna->stocked = 0;
	}
	return (dna->ret);
}
