/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flag.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 23:19:55 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/10 21:05:52 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

#define VALUE dna->flag.value
#define UNVALID "AEFGHIJKLMNPQRTVWYZaegkmnqrtvwy"

static void				init(t_dna *dna)
{
	dna->flag.f[0] = is_type;
	dna->flag.f[1] = is_int;
	dna->flag.f[2] = is_valid;
	dna->flag.f[3] = is_signed;
	dna->flag.f[4] = is_char;
	dna->flag.f[5] = is_addr;
	dna->flag.f[6] = ex_other;
	dna->flag.f[7] = ex_int;
	dna->flag.f[8] = ex_uint;
	dna->flag.f[9] = ex_char;
	dna->flag.f[10] = ex_string;
	dna->flag.f[11] = ex_addr;
	dna->flag.f[12] = ex_percent;
}

static void				clear(t_dna *dna)
{
	VALUE.tint = 0;
	VALUE.tuint = 0;
	VALUE.taddr = 0;
	dna->flag.modifier = 0;
	dna->flag.v_sign = 0;
	dna->flag.v_size = 0;
	dna->flag.v_padding = 0;
	dna->flag.s_sign = NULL;
	dna->flag.s_size = NULL;
	dna->flag.s_padding = NULL;
	dna->flag.s_flag = NULL;
	dna->flag.len = 0;
}

static void				clean(t_dna *dna)
{
	(dna->flag.s_sign) ? ft_strdel(&dna->flag.s_sign) : 0;
	(dna->flag.s_size) ? ft_strdel(&dna->flag.s_size) : 0;
	(dna->flag.s_padding) ? ft_strdel(&dna->flag.s_padding) : 0;
	(dna->flag.s_flag) ? ft_strdel(&dna->flag.s_flag) : 0;
}

bool					is_type(char *s)
{
	char				c;

	c = s[ft_strlen(s) - 1];
	while (*s)
	{
		if (ft_strchr(UNVALID, *s))
			return (false);
		s++;
	}
	return ((ft_strchr("dDioOuUxXbBcCsS", c)) ? true : false);
}

void					ft_flag(char *s, t_dna *dna)
{
	int					i;
	int					j;

	i = 0;
	j = 0;
	init(dna);
	clear(dna);
	get_modifier(s, dna);
	get_precision(s, dna);
	while (i < 6)
	{
		if ((*dna->flag.f[j])(s) == true)
			j = (i + i) + 1;
		else
			j = (i + i) + 2;
		i = j;
	}
	(*dna->flag.f[j])(s);
	set_flag(dna);
	clean(dna);
}
